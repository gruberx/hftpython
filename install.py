#rpy2 modules
import rpy2.rinterface
rpy2.rinterface.set_initoptions((b'rpy2', b'--no-save', b'--no-restore', b'--quiet'))
from rpy2.robjects.packages import importr

def main():
    utils = importr('utils')
    utils.install_packages("GetHFData")

if __name__ == "__main__":
      main()
