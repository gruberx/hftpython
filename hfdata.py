import rpy2.rinterface
rpy2.rinterface.set_initoptions((b'rpy2', b'--no-save', b'--no-restore', b'--quiet'))
from rpy2.robjects.functions import SignatureTranslatedFunction
from rpy2.robjects import numpy2ri, globalenv, NULL, pandas2ri, r
from rpy2.robjects.packages import importr
from rpy2.rinterface import RRuntimeError
from rpy2.robjects.methods import RS4
import rpy2.robjects as robjects
import pandas as pd

class HFTpy(object):
    def __init__(self):
        self.lib =  importr('GetHFData')
        self.df = pd.DataFrame()
        self.null = NULL
        pandas2ri.activate()

    def build_lob(self, dataframe, silent = True):
        self.df =  self.lib.ghfd_build_lob(dataframe)
        return self.df

    def download_file(self, ftp = "ftp://ftp.bmf.com.br/MarketData/BMF/NEG_BMF_20180702.zip", name = "example.zip"):
        self.lib.ghfd_download_file(ftp, name)

    def get_available_tickers_from_file(self, filename = "example.zip"):
        self.df = self.lib.ghfd_get_available_tickers_from_file(filename)
        return self.df

    def get_available_tickers_from_ftp(self, date, type_mkt, type_data):
        self.df = self.lib.ghfd_get_available_tickers_from_ftp(date, type_mkt, type_data = 'trades')
        return self.df

    def get_ftp_contents(self, mkt_type):
        self.df = self.lib.ghfd_get_ftp_contents(mkt_type, max_tries = 10, data_type = 'trades')
        return self.df

    def get_HF_data(self, my_assets = NULL,
                                                type_matching = 'exact',
                                                type_market = 'equity',
                                                type_data = 'trades',
                                                first_date = '2016-01-01',
                                                last_date = '2016-01-05',
                                                first_time =  NULL,
                                                last_time =  NULL,
                                                type_output = "agg",
                                                agg_diff = "15 min",
                                                dl_dir = "ftp files",
                                                max_dl_tries = 10,
                                                clean_files = False,
                                                only_dl = False):
        self.df =  self.lib.ghfd_get_HF_data(my_assets = my_assets,
                                                                                type_matching =  type_matching,
                                                                                type_market = type_market,
                                                                                type_data = type_data,
                                                                                first_date = first_date,
                                                                                last_date = last_date,
                                                                                first_time = first_time, 
                                                                                last_time = last_time,
                                                                                type_output = type_output,
                                                                                agg_diff = agg_diff,
                                                                                dl_dir = dl_dir,
                                                                                max_dl_tries = max_dl_tries,
                                                                                clean_files = clean_files,
                                                                                only_dl = only_dl)
        return self.df

    def read_file(self, type = "trades",  
                                                out_file = 'example.zip',
                                                  my_assets =  NULL,
                                                  type_matching = NULL,
                                                  type_data = 'trades',
                                                  first_time = '10:00:00',
                                                  last_time = '17:00:00',
                                                  type_output = 'agg',
                                                  agg_diff = '15 min'):
        self.read_files =  self.lib.ghfd_read_file(out_file = out_file,
                                                            my_assets =  my_assets,
                                                            type_matching = type_matching,
                                                            type_data = type_data,
                                                            first_time = first_time,
                                                            last_time = last_time,
                                                            type_output = type_output, 
                                                            agg_diff = agg_diff)
        return self.read_files