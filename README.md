# HFTPython
Um projeto simples feito para rodar o pacote [GetHFData](https://github.com/msperlin/GetHFData) em Python 3.0.x através do [rpy2 2.8.x](https://rpy2.readthedocs.io/en/version_2.8.x/).

# Requerimentos:
*  [R 3.2+ instalado](https://cran.r-project.org/bin/windows/base/) (Clique para baixar)
*  [Python 3.0.x+ instalado, recomendo o Anaconda](https://repo.anaconda.com/archive/Anaconda3-2019.03-Windows-x86_64.exe) (Clique aqui para baixar, x64)
*  Rpy2 (Ensino a instalar embaixo)

Em seguida, abra o terminal de comando/CMD e digite:

`conda install rpy2` se você instalou o Anaconda Python 3.x

ou 

`pip install rpy2` se você instalou o Python padrão.

E em seguida

`pip install tzlocal`

Abra na pasta aonde você baixou o repositório e digite:

`python install.py`, para ele instalar o pacote do GetHFData no seu ambiente global utilizado pelo Rpy2.

Após fazer isso, você pode testar o código usando o "example.py" que contém uma breve documentação.

Ou adicionar:

`from hfdata import HFTpy`

Atribuindo no código:

`hftpy = HFTpy()` para poder usar as funções adequadamente.

Eu simplesmente repliquei as funções [existentes na documentação em R](https://cran.r-project.org/web/packages/GetHFData/GetHFData.pdf)