from hfdata import HFTpy
import pandas as pd

def main():
    #Assigning Class
    hftpy = HFTpy()
    
    #Downloading FTP File
    ftp_address = "ftp://ftp.bmf.com.br/MarketData/BMF/NEG_BMF_20180702.zip"
    hftpy.download_file(ftp_address, "test.zip")
    #Show avaliable tickers
    tickers = hftpy.get_available_tickers_from_file("test.zip")
    #Read local file
    df = hftpy.read_file(out_file = "test.zip")
    #Download data from FTP
    df = hftpy.get_HF_data()
    #Build LOB
    lob = hftpy.build_lob(df)    
    #Get FTP contents:
    hftpy.get_ftp_contents('equities')

if __name__ == "__main__":
      main()
